﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDBTest.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDBTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        #region default API

       
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        #endregion

        #region MongoDB API

        //插入
        //GET api/values/InsertDate
        [HttpGet("InsertDate")]
        public JsonResult InsertDate()
        {
            ResultInfo res = new ResultInfo();

            try
            {
                //连接mongodb的固定格式，应该写在配置里面，由于这里做演示，所以单独写
                //user:user         第一个是用户名，第二个是密码
                //localhost:27017   是你数据库的连接
                //TestData          是你的数据库名称
                var mongodbClient = "mongodb://user:user@localhost:27017/TestData";  
                MongodbHost mongodb = new MongodbHost(mongodbClient);  

                mongodb.DataBase = "TestData";  //数据库名
                mongodb.Table = "empty_collection";  //操作的数据表（如果数据库没有定义表名，插入时会自动定义）

                List<ClassTable> ctList = new List<ClassTable>();
                int i = 0;
                while(i<3)
                {
                    ClassTable ct = new ClassTable();
                    switch (i)
                    {
                        case 0:
                            ct.ItemName = "班级";
                            ct.ItemValue = "取经天团";
                            break;
                        case 1:
                            ct.ItemName = "种族";
                            ct.ItemValue = "龙族";
                            break;
                        case 2:
                            ct.ItemName = "战斗力";
                            ct.ItemValue = "999999";
                            break;
                    }
                    ctList.Add(ct);
                    i++;
                }

                empty_collection model = new empty_collection()
                {
                    Name = "白龙马",
                    ClassTable = ctList,
                    CreateDate=DateTime.Now
                };
                List<empty_collection> Listmodel = new List<empty_collection>();
                Listmodel.Add(model);
                var resCount=TMongodbHelper<empty_collection>.InsertMany(mongodb, Listmodel);  // 插入多条数据

                res.code = resCount;
                res.message = resCount > 0 ? "操作成功" : "操作失败";
            }
            catch (Exception ex)
            {
                res.code = -2;
                res.message = "操作失败：" + ex.Message;
            }

            return new JsonResult(res);
        }

        //查询
        //GET api/values/GetDateList
        [HttpGet("GetDateList")]
        public JsonResult GetDateList()
        {
            ResultInfo res = new ResultInfo();
            try
            {
                //连接mongodb的固定格式，应该写在配置里面，由于这里做演示，所以单独写
                //user:user         第一个是用户名，第二个是密码
                //localhost:27017   是你数据库的连接
                //TestData          是你的数据库名称
                var mongodbClient = "mongodb://user:user@localhost:27017/TestData";
                MongodbHost mongodb = new MongodbHost(mongodbClient);

                mongodb.DataBase = "TestData";  //数据库名
                mongodb.Table = "empty_collection";  //操作的数据表


                #region 筛选条件（至少一个条件）

                var list = new List<FilterDefinition<empty_collection>>();
                //Gte 大于等于   Lte 小于等于
                list.Add(Builders<empty_collection>.Filter.Gte("CreateDate", "2021-01-01")); //注意：MongoDB当前方法查询需要必传至少一个参数，否则会报错
                var filter = Builders<empty_collection>.Filter.And(list);

                #endregion

                var GetList = TMongodbHelper<empty_collection>.FindList(mongodb, filter).ToList();    //查询全部
                //var GetList = TMongodbHelper<empty_collection>.FindListByPage(mongodb, filter, 1, 5, out count).ToList();   //分页查询
                res.code = 0;
                res.message = "查询成功";
                res.result = GetList;

            }
            catch (Exception ex)
            {
                res.code = -2;
                res.message = ex.Message;
            }
            return new JsonResult(res);
        }

        //修改
        //GET api/values/UpdateDate
        [HttpGet("UpdateDate")]
        public JsonResult UpdateDate()
        {
            ResultInfo res = new ResultInfo();
            try
            {
                //连接mongodb的固定格式，应该写在配置里面，由于这里做演示，所以单独写
                //user:user         第一个是用户名，第二个是密码
                //localhost:27017   是你数据库的连接
                //TestData          是你的数据库名称
                var mongodbClient = "mongodb://user:user@localhost:27017/TestData";
                MongodbHost mongodb = new MongodbHost(mongodbClient);

                mongodb.DataBase = "TestData";  //数据库名
                mongodb.Table = "empty_collection";  //操作的数据表


                //需要修改的条件
                var UpdateList = new List<FilterDefinition<empty_collection>>();
                UpdateList.Add(Builders<empty_collection>.Filter.Gte("CreateDate", "2021-01-01")); //注意：MongoDB当前方法查询需要必传至少一个参数，否则会报错
                UpdateList.Add(Builders<empty_collection>.Filter.Eq("Name", "白龙马"));
                var filterList = Builders<empty_collection>.Filter.And(UpdateList);

                //需要修改后的值
                Dictionary<string, string> dicList = new Dictionary<string, string>();
                dicList.Add("Name", "南无八部天龙广力菩萨");
                dicList.Add("CreateDate", DateTime.Now.ToString());

                var resCount = TMongodbHelper<empty_collection>.UpdateManay(mongodb, dicList, filterList);

                res.code = resCount.ModifiedCount > 0 ? 0 : -2;
                res.message = resCount.ModifiedCount > 0 ? "操作成功" : "操作失败";

            }
            catch (Exception ex)
            {
                res.message = "操作失败：" + ex.Message;
            }
            return new JsonResult(res);
        }

        //删除
        //GET api/values/DeleteDate
        [HttpGet("DeleteDate")]
        public JsonResult DeleteDate()
        {
            ResultInfo res = new ResultInfo();
            try
            {
                //连接mongodb的固定格式，应该写在配置里面，由于这里做演示，所以单独写
                //user:user         第一个是用户名，第二个是密码
                //localhost:27017   是你数据库的连接
                //TestData          是你的数据库名称
                var mongodbClient = "mongodb://user:user@localhost:27017/TestData";
                MongodbHost mongodb = new MongodbHost(mongodbClient);

                mongodb.DataBase = "TestData";  //数据库名
                mongodb.Table = "empty_collection";  //操作的数据表


                #region 筛选条件（至少一个条件）

                var list = new List<FilterDefinition<empty_collection>>();
                //Gte 大于等于   Lte 小于等于
                list.Add(Builders<empty_collection>.Filter.Gte("CreateDate", "2021-01-01")); //注意：MongoDB当前方法查询需要必传至少一个参数，否则会报错
                list.Add(Builders<empty_collection>.Filter.Eq("Name", "南无八部天龙广力菩萨"));
                var filter = Builders<empty_collection>.Filter.And(list);

                #endregion

                var resCount = TMongodbHelper<empty_collection>.DeleteMany(mongodb, filter);

                res.code = resCount.DeletedCount > 0 ? 0 : -2;
                res.message = resCount.DeletedCount > 0 ? "操作成功" : "操作失败";

            }
            catch (Exception ex)
            {
                res.message = "操作失败：" + ex.Message;
            }
            return new JsonResult(res);
        }


        #endregion

        #region Entity

        [BsonIgnoreExtraElements]  //忽略增加项（MongoDB是文档型数据库，所以数据字段是根据传入的数据自动增加的，如果不忽略，增加项，查询返回实体就少了字段报错，当然也可以查询指定字段）
        public class empty_collection  
        {
            public ObjectId _id { get; set; }  //主键
            public string Name { get; set; }   //姓名
            public List<ClassTable> ClassTable { get; set; }  //数据集合
            public DateTime CreateDate { get; set; }  //创建时间
        }

        public class ClassTable
        {
            public string ItemName { get; set; } //键名
            public string ItemValue { get; set; } //值
        }

        //返回类型实体
        public class ResultInfo
        {
            public int code { get; set; }
            public string message { get; set; }
            public object result { get; set; }
        }



        #endregion



    }
}
